; Commerce Deploy Core Makefile

api = 2
core = 7.x

; Contribs

projects[commerce][version] = 1.11
projects[commerce][subdir] = contrib

projects[commerce_features][version] = 1.1
projects[commerce_features][subdir] = contrib

projects[ctools][version] = 1.7
projects[ctools][subdir] = contrib

projects[date][version] = 2.8
projects[date][subdir] = contrib

projects[entity][version] = 1.6
projects[entity][subdir] = contrib

projects[entityreference][download][type] = git
projects[entityreference][download][branch] = 7.x-1.x
projects[entityreference][download][revision] = ab62b9a
projects[entityreference][subdir] = contrib
projects[entityreference][patch][1823406] = https://www.drupal.org/files/undefined_index-1823406-14.patch

projects[features][version] = 2.6
projects[features][subdir] = contrib

projects[libraries][version] = 2.2
projects[libraries][subdir] = contrib

projects[pathauto][version] = 1.2
projects[pathauto][subdir] = contrib

projects[rules][version] = 2.9
projects[rules][subdir] = contrib

projects[strongarm][version] = 2.0
projects[strongarm][subdir] = contrib

projects[token][version] = 1.6
projects[token][subdir] = contrib

projects[views][version] = 3.11
projects[views][subdir] = contrib

projects[views_bulk_operations][version] = 3.3
projects[views_bulk_operations][subdir] = contrib

; Development tools

projects[devel][version] = 1.5
projects[devel][subdir] = contrib

projects[commerce_devel][download][type] = git
projects[commerce_devel][download][revision] = 79e7b71
projects[commerce_devel][download][branch] = 7.x-1.x
projects[commerce_devel][subdir] = contrib
